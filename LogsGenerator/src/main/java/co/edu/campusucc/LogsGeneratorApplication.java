package co.edu.campusucc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Principal class for creating logs files relate to the simulation an Academic System Information.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
@SpringBootApplication
public class LogsGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogsGeneratorApplication.class, args);
	}
}
