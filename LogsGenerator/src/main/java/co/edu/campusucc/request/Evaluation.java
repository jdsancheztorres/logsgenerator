package co.edu.campusucc.request;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
/**
 * Evaluation class associated to the simulation of logs.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class Evaluation {
	private int evaluationId;
	private String evaluationDate;
	private String name;
	private String qualification;
	private Subject subject;
	private Student student;
	private Teacher teacher;
}
