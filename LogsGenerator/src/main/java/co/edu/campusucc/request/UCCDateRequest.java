package co.edu.campusucc.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * UCCDateRequest class associated to the simulation of logs.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class UCCDateRequest {
	
	private int day;
	private int month;
	private int year;
	private int hour;
	private int minute;
	private int second;
}
