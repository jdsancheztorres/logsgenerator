package co.edu.campusucc.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * Subject class associated to the simulation of logs.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class Subject {
	private int id;
	private String description;
}
