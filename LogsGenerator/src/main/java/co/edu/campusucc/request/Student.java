package co.edu.campusucc.request;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * Student class associated to the simulation of logs.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class Student {
	private int idStudent;
	private String DocumentType;
	private String name;
	private String lastName;
	private String telephone;
	private String address;
	private List<Subject> subjectList;
}
