package co.edu.campusucc.request;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
/**
 * Teacher class associated to the simulation of logs.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class Teacher {
	private int idTeacher;
	private String DocumentType;
	private String name;
	private String lastName;
	private String bachelor;
	private String masterDegree;
	private List<Subject> subjectList;
}
