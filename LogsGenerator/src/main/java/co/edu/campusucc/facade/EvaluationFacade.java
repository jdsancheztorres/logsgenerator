package co.edu.campusucc.facade;

import java.util.ArrayList;
import java.util.List;

import co.edu.campusucc.Util;
import co.edu.campusucc.request.Evaluation;
import co.edu.campusucc.request.Student;
import co.edu.campusucc.request.Subject;
import co.edu.campusucc.request.Teacher;
import co.edu.campusucc.response.ErrorUCC;
import co.edu.campusucc.response.ResponseData;
import co.edu.campusucc.response.ResponseHeader;
import co.edu.campusucc.response.ResponseUCC;
/**
 * Evaluation Facade associated to the simulation of logs.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class EvaluationFacade {
	
	private static List<Evaluation> evaluationList = new ArrayList<Evaluation>();
	private static List<ResponseUCC> responseList = new ArrayList<ResponseUCC>();
	
	
	public static Evaluation createRequest() {
		loadData();
		int indexRandom = (int) (Math.random() * EvaluationFacade.evaluationList.size());
		
		return EvaluationFacade.evaluationList.get(indexRandom);
	}
	
	public static ResponseUCC createResponse() {
		loadDataResponse();
		int indexRandom = (int) (Math.random() * EvaluationFacade.responseList.size());
		return EvaluationFacade.responseList.get(indexRandom);
	}
	
	private static void loadData() {
		loadEvaluation1();
		loadEvaluation2();
		loadEvaluation3();
		loadEvaluation4();
		loadEvaluation5();
		loadEvaluation6();
		loadEvaluation7();
		loadEvaluation8();
		loadEvaluation9();
	}
	
	private static void loadEvaluation1() {
		Evaluation evaluation1 = new Evaluation();
		evaluation1.setEvaluationDate(Util.convertDateToString());
		evaluation1.setEvaluationId(1);
		evaluation1.setName("Evaluacion 1");
		evaluation1.setQualification("4.5");
		evaluation1.setStudent(null);
		Student student1 = new Student();
		student1.setAddress("cra 13 32 12");
		student1.setDocumentType("CC");
		student1.setIdStudent(1);
		student1.setLastName("Vargas");
		student1.setName("Martina");
		student1.setTelephone("3132342333");
		evaluation1.setStudent(student1);
		Teacher teacher1 = new Teacher();
		teacher1.setBachelor("Ingeniero de Sistemas");
		teacher1.setDocumentType("CC");
		teacher1.setIdTeacher(11);
		teacher1.setLastName("Torres");
		teacher1.setName("Abel");
		evaluation1.setTeacher(teacher1);
		Subject subject1 = new Subject();
		subject1.setId(1);
		subject1.setDescription("Gerencia de Proyectos de Tecnologia");
		evaluation1.setSubject(subject1);
		EvaluationFacade.evaluationList.add(evaluation1);
	}
	
	private static void loadEvaluation2() {
		Evaluation evaluation2 = new Evaluation();
		evaluation2.setEvaluationDate(Util.convertDateToString());
		evaluation2.setEvaluationId(1);
		evaluation2.setName("Evaluacion 1");
		evaluation2.setQualification("3.5");
		Student student2 = new Student();
		student2.setAddress("cra 56 42 24");
		student2.setDocumentType("CC");
		student2.setIdStudent(4);
		student2.setLastName("Romero");
		student2.setName("Brenda");
		student2.setTelephone("3182174238");
		evaluation2.setStudent(student2);
		Teacher teacher1 = new Teacher();
		teacher1.setBachelor("Ingeniero de Sistemas");
		teacher1.setDocumentType("CC");
		teacher1.setIdTeacher(11);
		teacher1.setLastName("Torres");
		teacher1.setName("Abel");
		evaluation2.setTeacher(teacher1);
		Subject subject1 = new Subject();
		subject1.setId(1);
		subject1.setDescription("Gerencia de Proyectos de Tecnologia");
		evaluation2.setSubject(subject1);
		EvaluationFacade.evaluationList.add(evaluation2);
	}
	
	private static void loadEvaluation3() {
		Evaluation evaluation3 = new Evaluation();
		evaluation3.setEvaluationDate(Util.convertDateToString());
		evaluation3.setEvaluationId(1);
		evaluation3.setName("Evaluacion 1");
		evaluation3.setQualification("2.7");
		Student student3 = new Student();
		student3.setAddress("cra 13 32 12");
		student3.setDocumentType("CC");
		student3.setIdStudent(1);
		student3.setLastName("Vargas");
		student3.setName("Martina");
		student3.setTelephone("3132342333");
		evaluation3.setStudent(student3);
		Teacher teacher1 = new Teacher();
		teacher1.setBachelor("Ingeniero de Sistemas");
		teacher1.setDocumentType("CC");
		teacher1.setIdTeacher(11);
		teacher1.setLastName("Torres");
		teacher1.setName("Abel");
		evaluation3.setTeacher(teacher1);
		Subject subject1 = new Subject();
		subject1.setId(1);
		subject1.setDescription("Gerencia de Proyectos de Tecnologia");
		evaluation3.setSubject(subject1);
		EvaluationFacade.evaluationList.add(evaluation3);
	}
	
	private static void loadEvaluation4() {
		Evaluation evaluation4 = new Evaluation();
		evaluation4.setEvaluationDate(Util.convertDateToString());
		evaluation4.setEvaluationId(1);
		evaluation4.setName("Evaluacion 1");
		evaluation4.setQualification("2.7");
		Student student4 = new Student();
		student4.setAddress("cra 13 32 12");
		student4.setDocumentType("CC");
		student4.setIdStudent(1);
		student4.setLastName("Vargas");
		student4.setName("Martina");
		student4.setTelephone("3132342333");
		evaluation4.setStudent(student4);
		Teacher teacher1 = new Teacher();
		teacher1.setBachelor("Ingeniero de Sistemas");
		teacher1.setDocumentType("CC");
		teacher1.setIdTeacher(11);
		teacher1.setLastName("Torres");
		teacher1.setName("Abel");
		evaluation4.setTeacher(teacher1);
		Subject subject1 = new Subject();
		subject1.setId(1);
		subject1.setDescription("Gerencia de Proyectos de Tecnologia");
		evaluation4.setSubject(subject1);
		EvaluationFacade.evaluationList.add(evaluation4);
	}
	
	private static void loadEvaluation5() {
		Evaluation evaluation5 = new Evaluation();
		evaluation5.setEvaluationDate(Util.convertDateToString());
		evaluation5.setEvaluationId(1);
		evaluation5.setName("Evaluacion 1");
		evaluation5.setQualification("2.7");
		Student student5 = new Student();
		student5.setAddress("cra 13 32 12");
		student5.setDocumentType("CC");
		student5.setIdStudent(1);
		student5.setLastName("Vargas");
		student5.setName("Martina");
		student5.setTelephone("3132342333");
		evaluation5.setStudent(student5);
		Teacher teacher1 = new Teacher();
		teacher1.setBachelor("Ingeniero de Sistemas");
		teacher1.setDocumentType("CC");
		teacher1.setIdTeacher(11);
		teacher1.setLastName("Torres");
		teacher1.setName("Abel");
		evaluation5.setTeacher(teacher1);
		Subject subject1 = new Subject();
		subject1.setId(1);
		subject1.setDescription("Gerencia de Proyectos de Tecnologia");
		evaluation5.setSubject(subject1);
		EvaluationFacade.evaluationList.add(evaluation5);
	}
	
	private static void loadEvaluation6() {
		Evaluation evaluation6 = new Evaluation();
		evaluation6.setEvaluationDate(Util.convertDateToString());
		evaluation6.setEvaluationId(1);
		evaluation6.setName("Evaluacion 1");
		evaluation6.setQualification("2.7");
		Student student6 = new Student();
		student6.setAddress("cra 13 32 13");
		student6.setDocumentType("CC");
		student6.setIdStudent(1);
		student6.setLastName("Giraldo");
		student6.setName("Rodrigo");
		student6.setTelephone("3173214123");
		evaluation6.setStudent(student6);
		Teacher teacher1 = new Teacher();
		teacher1.setBachelor("Ingeniero de Sistemas");
		teacher1.setDocumentType("CC");
		teacher1.setIdTeacher(11);
		teacher1.setLastName("Torres");
		teacher1.setName("Abel");
		evaluation6.setTeacher(teacher1);
		Subject subject1 = new Subject();
		subject1.setId(1);
		subject1.setDescription("Gerencia de Proyectos de Tecnologia");
		evaluation6.setSubject(subject1);
		EvaluationFacade.evaluationList.add(evaluation6);
	}
	
	private static void loadEvaluation7() {
		Evaluation evaluation7 = new Evaluation();
		evaluation7.setEvaluationDate(Util.convertDateToString());
		evaluation7.setEvaluationId(1);
		evaluation7.setName("Evaluacion 1");
		evaluation7.setQualification("2.7");
		Student student7 = new Student();
		student7.setAddress("cra 13 32 13");
		student7.setDocumentType("CC");
		student7.setIdStudent(1);
		student7.setLastName("Giraldo");
		student7.setName("Rodrigo");
		student7.setTelephone("3173214123");
		evaluation7.setStudent(student7);
		Teacher teacher1 = new Teacher();
		teacher1.setBachelor("Ingeniero de Sistemas");
		teacher1.setDocumentType("CC");
		teacher1.setIdTeacher(11);
		teacher1.setLastName("Torres");
		teacher1.setName("Abel");
		evaluation7.setTeacher(teacher1);
		Subject subject1 = new Subject();
		subject1.setId(1);
		subject1.setDescription("Gerencia de Proyectos de Tecnologia");
		evaluation7.setSubject(subject1);
		EvaluationFacade.evaluationList.add(evaluation7);
	}
	
	private static void loadEvaluation8() {
		Evaluation evaluation8 = new Evaluation();
		evaluation8.setEvaluationDate(Util.convertDateToString());
		evaluation8.setEvaluationId(1);
		evaluation8.setName("Evaluacion 1");
		evaluation8.setQualification("3.1");
		Student student8 = new Student();
		student8.setAddress("cra 7 234 24");
		student8.setDocumentType("CC");
		student8.setIdStudent(4);
		student8.setLastName("Velazquez");
		student8.setName("Cain");
		student8.setTelephone("31723456432");
		evaluation8.setStudent(student8);
		Teacher teacher1 = new Teacher();
		teacher1.setBachelor("Ingeniero de Sistemas");
		teacher1.setDocumentType("CC");
		teacher1.setIdTeacher(11);
		teacher1.setLastName("Torres");
		teacher1.setName("Abel");
		evaluation8.setTeacher(teacher1);
		Subject subject1 = new Subject();
		subject1.setId(1);
		subject1.setDescription("Gerencia de Proyectos de Tecnologia");
		evaluation8.setSubject(subject1);
		EvaluationFacade.evaluationList.add(evaluation8);
	}
	
	private static void loadEvaluation9() {
		Evaluation evaluation9 = new Evaluation();
		evaluation9.setEvaluationDate(Util.convertDateToString());
		evaluation9.setEvaluationId(1);
		evaluation9.setName("Evaluacion 1");
		Student student4 = new Student();
		student4.setAddress("cra 1 0 13");
		student4.setDocumentType("CC");
		student4.setIdStudent(4);
		student4.setLastName("Coulet");
		student4.setName("Daniela");
		student4.setTelephone("313232453");
		evaluation9.setStudent(student4);
		Teacher teacher3 = new Teacher();
		teacher3.setBachelor("Ingeniero de Sistemas y Computacion");
		teacher3.setDocumentType("CC");
		teacher3.setIdTeacher(13);
		teacher3.setLastName("Serna");
		teacher3.setName("Juan");
		evaluation9.setTeacher(teacher3);
		Subject subject41 = new Subject();
		subject41.setId(4);
		subject41.setDescription("Paradigmas de investigación");
		evaluation9.setSubject(subject41);
		EvaluationFacade.evaluationList.add(evaluation9);
	}
	
	private static void loadDataResponse() {
		
		ResponseUCC response = new ResponseUCC();
		ResponseHeader responseHeader = new ResponseHeader();
		responseHeader.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader.setStatus("200");
		response.setResponseHeaderObject(responseHeader);
		response.setResponseStatus("ok");
		ResponseData responseData = new ResponseData();
		responseData.setErrors(null);
		response.setResponseDataObject(responseData);
		responseList.add(response);
		
		ResponseUCC response2 = new ResponseUCC();
		ResponseHeader responseHeader2 = new ResponseHeader();
		responseHeader2.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader2.setStatus("400");
		response2.setResponseHeaderObject(responseHeader);
		response2.setResponseStatus("fail");
		ResponseData responseData2 = new ResponseData();
		ErrorUCC error = new ErrorUCC();
		error.setCode("400");
		error.setMessage("Bad Request");
		ArrayList<ErrorUCC> errorList = new ArrayList<ErrorUCC>();
		errorList.add(error);
		responseData2.setErrors(errorList);
		response2.setResponseDataObject(responseData);
		responseList.add(response2);
		
		ResponseUCC response3 = new ResponseUCC();
		ResponseHeader responseHeader3 = new ResponseHeader();
		responseHeader3.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader3.setStatus("400");
		response3.setResponseHeaderObject(responseHeader);
		response3.setResponseStatus("fail");
		ResponseData responseData3 = new ResponseData();
		ErrorUCC error3 = new ErrorUCC();
		error3.setCode("400");
		error3.setMessage("Bad Request");
		ArrayList<ErrorUCC> errorList3 = new ArrayList<ErrorUCC>();
		errorList3.add(error3);
		responseData3.setErrors(errorList);
		response3.setResponseDataObject(responseData2);
		responseList.add(response3);
		
		ResponseUCC response4 = new ResponseUCC();
		ResponseHeader responseHeader4 = new ResponseHeader();
		responseHeader4.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader4.setStatus("400");
		response3.setResponseHeaderObject(responseHeader);
		response3.setResponseStatus("fail");
		ResponseData responseData4 = new ResponseData();
		ErrorUCC error4 = new ErrorUCC();
		error4.setCode("401");
		error4.setMessage("Unauthorized");
		ArrayList<ErrorUCC> errorList4 = new ArrayList<ErrorUCC>();
		errorList4.add(error4);
		responseData4.setErrors(errorList);
		response4.setResponseDataObject(responseData4);
		responseList.add(response4);
		
		ResponseUCC response5 = new ResponseUCC();
		ResponseHeader responseHeader5 = new ResponseHeader();
		responseHeader5.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader5.setStatus("400");
		response5.setResponseHeaderObject(responseHeader);
		response5.setResponseStatus("fail");
		ResponseData responseData5 = new ResponseData();
		ErrorUCC error5 = new ErrorUCC();
		error5.setCode("403");
		error5.setMessage("Forbidden");
		ArrayList<ErrorUCC> errorList5 = new ArrayList<ErrorUCC>();
		errorList5.add(error4);
		responseData5.setErrors(errorList5);
		response5.setResponseDataObject(responseData5);
		responseList.add(response5);
		
		ResponseUCC response6 = new ResponseUCC();
		ResponseHeader responseHeader6 = new ResponseHeader();
		responseHeader6.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader6.setStatus("400");
		response6.setResponseHeaderObject(responseHeader);
		response6.setResponseStatus("fail");
		ResponseData responseData6 = new ResponseData();
		ErrorUCC error6 = new ErrorUCC();
		error6.setCode("403");
		error6.setMessage("Forbidden");
		ArrayList<ErrorUCC> errorList6 = new ArrayList<ErrorUCC>();
		errorList6.add(error6);
		responseData6.setErrors(errorList6);
		response6.setResponseDataObject(responseData6);
		responseList.add(response6);
		
		ResponseUCC response7 = new ResponseUCC();
		ResponseHeader responseHeader7 = new ResponseHeader();
		responseHeader7.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader7.setStatus("404");
		response7.setResponseHeaderObject(responseHeader7);
		response7.setResponseStatus("fail");
		ResponseData responseData7 = new ResponseData();
		ErrorUCC error7 = new ErrorUCC();
		error7.setCode("404");
		error7.setMessage("Not Found");
		ArrayList<ErrorUCC> errorList7 = new ArrayList<ErrorUCC>();
		errorList7.add(error7);
		responseData7.setErrors(errorList7);
		response7.setResponseDataObject(responseData7);
		responseList.add(response7);
		
		ResponseUCC response88 = new ResponseUCC();
		ResponseHeader responseHeader88 = new ResponseHeader();
		responseHeader88.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader88.setStatus("405");
		response88.setResponseHeaderObject(responseHeader88);
		response88.setResponseStatus("fail");
		ResponseData responseData88 = new ResponseData();
		ErrorUCC error88 = new ErrorUCC();
		error88.setCode("405");
		error88.setMessage("Method Not Allowed");
		ArrayList<ErrorUCC> errorList88 = new ArrayList<ErrorUCC>();
		errorList88.add(error88);
		responseData88.setErrors(errorList88);
		response88.setResponseDataObject(responseData88);
		responseList.add(response88);
		
		ResponseUCC response8 = new ResponseUCC();
		ResponseHeader responseHeader8 = new ResponseHeader();
		responseHeader8.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader8.setStatus("415");
		response8.setResponseHeaderObject(responseHeader8);
		response8.setResponseStatus("fail");
		ResponseData responseData8 = new ResponseData();
		ErrorUCC error8 = new ErrorUCC();
		error8.setCode("415");
		error8.setMessage("Unsupported Media Type");
		ArrayList<ErrorUCC> errorList8 = new ArrayList<ErrorUCC>();
		errorList8.add(error8);
		responseData8.setErrors(errorList8);
		response8.setResponseDataObject(responseData8);
		responseList.add(response8);
		
		ResponseUCC response9 = new ResponseUCC();
		ResponseHeader responseHeader9 = new ResponseHeader();
		responseHeader9.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader9.setStatus("500");
		response9.setResponseHeaderObject(responseHeader8);
		response9.setResponseStatus("fail");
		ResponseData responseData9 = new ResponseData();
		ErrorUCC error9 = new ErrorUCC();
		error9.setCode("description");
		error9.setCode("500");
		error9.setMessage("This field is required");
		ArrayList<ErrorUCC> errorList9 = new ArrayList<ErrorUCC>();
		errorList9.add(error9);
		responseData9.setErrors(errorList9);
		response9.setResponseDataObject(responseData9);
		responseList.add(response9);
		
		ResponseUCC response91 = new ResponseUCC();
		ResponseHeader responseHeader91 = new ResponseHeader();
		responseHeader91.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader91.setStatus("500");
		response91.setResponseHeaderObject(responseHeader8);
		response91.setResponseStatus("fail");
		ResponseData responseData91 = new ResponseData();
		ErrorUCC error911 = new ErrorUCC();
		error911.setCode("evaluationDate");
		error911.setCode("100");
		error911.setMessage("This field is required");
		ErrorUCC error912 = new ErrorUCC();
		error912.setCode("name");
		error912.setCode("100");
		error912.setMessage("This field is required");
		ErrorUCC error913 = new ErrorUCC();
		error913.setCode("qualification");
		error912.setCode("100");
		error912.setMessage("This field is incorrect");
		ArrayList<ErrorUCC> errorList91 = new ArrayList<ErrorUCC>();
		errorList91.add(error911);
		errorList91.add(error912);
		errorList91.add(error913);
		responseData9.setErrors(errorList91);
		response9.setResponseDataObject(responseData91);
		responseList.add(response91);
		
		ResponseUCC response92 = new ResponseUCC();
		ResponseHeader responseHeader92 = new ResponseHeader();
		responseHeader92.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader92.setStatus("500");
		response92.setResponseHeaderObject(responseHeader8);
		response92.setResponseStatus("fail");
		ResponseData responseData92 = new ResponseData();
		ErrorUCC error922 = new ErrorUCC();
		error912.setCode("name");
		error912.setCode("100");
		error912.setMessage("This field is required");
		ErrorUCC error923 = new ErrorUCC();
		error913.setCode("qualification");
		error912.setCode("100");
		error912.setMessage("This field is incorrect");
		ArrayList<ErrorUCC> errorList92 = new ArrayList<ErrorUCC>();
		errorList92.add(error922);
		errorList92.add(error923);
		responseData9.setErrors(errorList92);
		response92.setResponseDataObject(responseData92);
		responseList.add(response92);
		

		ResponseUCC response10 = new ResponseUCC();
		ResponseHeader responseHeader10 = new ResponseHeader();
		responseHeader10.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader10.setStatus("502");
		response10.setResponseHeaderObject(responseHeader10);
		response10.setResponseStatus("fail");
		ResponseData responseData10 = new ResponseData();
		ErrorUCC error10 = new ErrorUCC();
		error10.setCode("502");
		error10.setMessage("Bad Gateway");
		ArrayList<ErrorUCC> errorList10 = new ArrayList<ErrorUCC>();
		errorList10.add(error10);
		responseData10.setErrors(errorList10);
		response10.setResponseDataObject(responseData10);
		responseList.add(response10);
		
		ResponseUCC response11 = new ResponseUCC();
		ResponseHeader responseHeader11 = new ResponseHeader();
		responseHeader11.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader11.setStatus("503");
		response11.setResponseHeaderObject(responseHeader11);
		response11.setResponseStatus("fail");
		ResponseData responseData11 = new ResponseData();
		ErrorUCC error11 = new ErrorUCC();
		error11.setCode("503");
		error11.setMessage("Service Unavailable.");
		ArrayList<ErrorUCC> errorList11 = new ArrayList<ErrorUCC>();
		errorList11.add(error11);
		responseData11.setErrors(errorList11);
		response11.setResponseDataObject(responseData11);
		responseList.add(response11);
		
	}
	
}
