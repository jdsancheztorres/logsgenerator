package co.edu.campusucc.facade.teacher;

import java.util.ArrayList;
import java.util.List;

import co.edu.campusucc.request.Subject;
import co.edu.campusucc.request.Teacher;
/**
 * Teacher Facade associated to the simulation of logs about listing 
 * all teachers that belong to the IT Management master' degree from the UCC.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class TeacherFacade {

	private static List<Teacher> teacherList = new ArrayList<Teacher>();
	
	public static Teacher createRequest() {
		loadData();
		int indexRandom = (int) (Math.random() * TeacherFacade.teacherList.size());
		
		return TeacherFacade.teacherList.get(indexRandom);
	}
	
	private static void loadData() {
		Teacher teacher1 = new Teacher();
		teacher1.setBachelor("Ingeniero de Sistemas");
		teacher1.setDocumentType("CC");
		teacher1.setIdTeacher(11);
		teacher1.setLastName("Torres");
		teacher1.setName("Abel");
		List<Subject> subjectList = new ArrayList<Subject>();
		Subject subject = new Subject();
		subject.setId(1);
		subject.setDescription("Gerencia de Proyectos de Tecnologia");
		subjectList.add(subject);
		teacher1.setSubjectList(subjectList);
		TeacherFacade.teacherList.add(teacher1);
		
		Teacher Teacher2 = new Teacher();
		Teacher2.setBachelor("Ingeniero de Sistemas");
		Teacher2.setDocumentType("CC");
		Teacher2.setIdTeacher(12);
		Teacher2.setLastName("Naranjo");
		Teacher2.setName("Monica");
		List<Subject> subjectList2 = new ArrayList<Subject>();
		Subject subject2 = new Subject();
		subject2.setId(2);
		subject2.setDescription("Gobernabilidad de TI");
		subjectList2.add(subject2);
		Subject subject22 = new Subject();
		subject22.setId(2);
		subject22.setDescription("Tendencias Organizacionales");
		subjectList2.add(subject22);
		Teacher2.setSubjectList(subjectList2);
		TeacherFacade.teacherList.add(Teacher2);
		
		Teacher Teacher3 = new Teacher();
		Teacher2.setBachelor("Ingeniero de Sistemas y Computacion");
		Teacher3.setDocumentType("CC");
		Teacher3.setIdTeacher(13);
		Teacher3.setLastName("Serna");
		Teacher3.setName("Juan");
		List<Subject> subjectList3 = new ArrayList<Subject>();
		Subject subject3 = new Subject();
		subject3.setId(2);
		subject3.setDescription("Gobernabilidad de TI");
		subjectList3.add(subject3);
		Subject subject33 = new Subject();
		subject33.setId(4);
		subject33.setDescription("Paradigmas de investigación");
		subjectList3.add(subject33);
		Teacher3.setSubjectList(subjectList3);
		TeacherFacade.teacherList.add(Teacher3);
		
		Teacher Teacher4 = new Teacher();
		Teacher4.setBachelor("Ingeniero de Sistemas y Computacion");
		Teacher4.setDocumentType("CC");
		Teacher4.setIdTeacher(14);
		Teacher4.setLastName("Galindo");
		Teacher4.setName("Monica");
		List<Subject> subjectList4 = new ArrayList<Subject>();
		Subject subject4 = new Subject();
		subject4.setId(2);
		subject4.setDescription("Gobernabilidad de TI");
		subjectList4.add(subject4);
		Subject subject41 = new Subject();
		subject41.setId(4);
		subject41.setDescription("Paradigmas de investigación");
		subjectList4.add(subject41);
		Teacher3.setSubjectList(subjectList4);
		Subject subject42 = new Subject();
		subject42.setId(5);
		subject42.setDescription("Gestión de Riesgo de TI");
		subjectList4.add(subject42);
		Teacher4.setSubjectList(subjectList4);
		TeacherFacade.teacherList.add(Teacher4);
		
		Teacher Teacher5 = new Teacher();
		Teacher5.setBachelor("Ingeniero Informático");
		Teacher5.setDocumentType("CC");
		Teacher5.setIdTeacher(15);
		Teacher5.setLastName("Rodriguez");
		Teacher5.setName("Wilson");
		List<Subject> subjectList5 = new ArrayList<Subject>();
		Subject subject5 = new Subject();
		subject5.setId(5);
		subject5.setDescription("Diseño de proyectos de interventoria");
		subjectList5.add(subject5);
		Teacher5.setSubjectList(subjectList5);
		TeacherFacade.teacherList.add(Teacher5);
		
		Teacher Teacher6 = new Teacher();
		Teacher6.setBachelor("Ingeniera Industrial");
		Teacher6.setDocumentType("CC");
		Teacher6.setIdTeacher(16);
		Teacher6.setLastName("Gonzalez");
		Teacher6.setName("Mariela");
		List<Subject> subjectList6 = new ArrayList<Subject>();
		Subject subject6 = new Subject();
		subject6.setId(5);
		subject6.setDescription("Diseño de proyectos de interventoria");
		subjectList6.add(subject6);
		Teacher6.setSubjectList(subjectList6);
		TeacherFacade.teacherList.add(Teacher6);
	}
}
