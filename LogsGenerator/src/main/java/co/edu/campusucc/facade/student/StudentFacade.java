package co.edu.campusucc.facade.student;

import java.util.ArrayList;
import java.util.List;

import co.edu.campusucc.request.Student;
import co.edu.campusucc.request.Subject;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
/**
 * Student Facade associated to the simulation of about information regarding to students 
 * of the UCC.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class StudentFacade {
	
	private static List<Student> studentList = new ArrayList<Student>();
		
	public static Student createRequest() {
		loadData();
		int indexRandom = (int) (Math.random() * StudentFacade.studentList.size());
		
		return StudentFacade.studentList.get(indexRandom);
	}
	
	private static void loadData() {
		Student student1 = new Student();
		student1.setAddress("cra 13 32 12");
		student1.setDocumentType("CC");
		student1.setIdStudent(1);
		student1.setLastName("Vargas");
		student1.setName("Martina");
		student1.setTelephone("3132342333");
		List<Subject> subjectList = new ArrayList<Subject>();
		Subject subject = new Subject();
		subject.setId(1);
		subject.setDescription("Gerencia de Proyectos de Tecnologia");
		subjectList.add(subject);
		student1.setSubjectList(subjectList);
		StudentFacade.studentList.add(student1);
		
		Student student2 = new Student();
		student2.setAddress("cra 13 32 13");
		student2.setDocumentType("CC");
		student2.setIdStudent(1);
		student2.setLastName("Giraldo");
		student2.setName("Rodrigo");
		student2.setTelephone("3173214123");
		List<Subject> subjectList2 = new ArrayList<Subject>();
		Subject subject2 = new Subject();
		subject2.setId(2);
		subject2.setDescription("Gobernabilidad de TI");
		subjectList2.add(subject2);
		Subject subject22 = new Subject();
		subject22.setId(2);
		subject22.setDescription("Tendencias Organizacionales");
		subjectList2.add(subject22);
		student2.setSubjectList(subjectList2);
		Subject subject23 = new Subject();
		subject23.setId(1);
		subject23.setDescription("Gerencia de Proyectos de Tecnologia");
		subjectList2.add(subject23);
		StudentFacade.studentList.add(student2);
		
		Student student3 = new Student();
		student3.setAddress("cra 1 0 13");
		student3.setDocumentType("CC");
		student3.setIdStudent(3);
		student3.setLastName("Coulet");
		student3.setName("Daniela");
		student3.setTelephone("313232453");
		List<Subject> subjectList3 = new ArrayList<Subject>();
		Subject subject3 = new Subject();
		subject3.setId(2);
		subject3.setDescription("Gobernabilidad de TI");
		subjectList3.add(subject3);
		Subject subject33 = new Subject();
		subject33.setId(4);
		subject33.setDescription("Paradigmas de investigación");
		subjectList3.add(subject33);
		student3.setSubjectList(subjectList3);
		StudentFacade.studentList.add(student3);
		
		Student student4 = new Student();
		student4.setAddress("cra 1 0 13");
		student4.setDocumentType("CC");
		student4.setIdStudent(4);
		student4.setLastName("Coulet");
		student4.setName("Daniela");
		student4.setTelephone("313232453");
		List<Subject> subjectList4 = new ArrayList<Subject>();
		Subject subject4 = new Subject();
		subject4.setId(2);
		subject4.setDescription("Gobernabilidad de TI");
		subjectList4.add(subject4);
		Subject subject41 = new Subject();
		subject41.setId(4);
		subject41.setDescription("Paradigmas de investigación");
		subjectList4.add(subject41);
		student3.setSubjectList(subjectList4);
		Subject subject42 = new Subject();
		subject42.setId(5);
		subject42.setDescription("Gestión de Riesgo de TI");
		subjectList4.add(subject42);
		student4.setSubjectList(subjectList4);
		StudentFacade.studentList.add(student4);
		
		Student student5 = new Student();
		student5.setAddress("cll 5 7 13");
		student5.setDocumentType("CC");
		student5.setIdStudent(5);
		student5.setLastName("Mesa");
		student5.setName("Carlos");
		student5.setTelephone("3172464354");
		List<Subject> subjectList5 = new ArrayList<Subject>();
		Subject subject5 = new Subject();
		subject5.setId(5);
		subject5.setDescription("Diseño de proyectos de interventoria");
		subjectList5.add(subject5);
		student5.setSubjectList(subjectList5);
		StudentFacade.studentList.add(student5);
		
		Student student6 = new Student();
		student6.setAddress("cll 5 7 13");
		student6.setDocumentType("CC");
		student6.setIdStudent(4);
		student6.setLastName("Coulet");
		student6.setName("Daniela");
		student6.setTelephone("313232453");
		List<Subject> subjectList6 = new ArrayList<Subject>();
		Subject subject6 = new Subject();
		subject6.setId(5);
		subject6.setDescription("Diseño de proyectos de interventoria");
		subjectList6.add(subject6);
		student6.setSubjectList(subjectList6);
		StudentFacade.studentList.add(student6);
		
		Student student7 = new Student();
		student7.setAddress("cra 56 42 24");
		student7.setDocumentType("CC");
		student7.setIdStudent(4);
		student7.setLastName("Romero");
		student7.setName("Brenda");
		student7.setTelephone("3182174238");
		List<Subject> subjectList7 = new ArrayList<Subject>();
		Subject subject7 = new Subject();
		subject7.setId(5);
		subject7.setDescription("Diseño de proyectos de interventoria");
		subjectList7.add(subject7);
		student7.setSubjectList(subjectList7);
		Subject subject71 = new Subject();
		subject71.setId(4);
		subject71.setDescription("Paradigmas de investigación");
		subjectList7.add(subject71);
		Subject subject72 = new Subject();
		subject72.setId(1);
		subject72.setDescription("Gerencia de Proyectos de Tecnologia");
		subjectList.add(subject72);
		StudentFacade.studentList.add(student7);
		student7.setSubjectList(subjectList7);
		
		Student student8 = new Student();
		student8.setAddress("cra 7 234 24");
		student8.setDocumentType("CC");
		student8.setIdStudent(4);
		student8.setLastName("Velazquez");
		student8.setName("Cain");
		student8.setTelephone("31723456432");
		List<Subject> subjectList8 = new ArrayList<Subject>();
		Subject subject8 = new Subject();
		subject8.setId(5);
		subject8.setDescription("Diseño de proyectos de interventoria");
		subjectList8.add(subject8);
		student8.setSubjectList(subjectList8);
		Subject subject81 = new Subject();
		subject81.setId(4);
		subject81.setDescription("Paradigmas de investigación");
		subjectList8.add(subject71);
		Subject subject82 = new Subject();
		subject82.setId(1);
		subject82.setDescription("Gerencia de Proyectos de Tecnologia");
		subjectList.add(subject82);
		StudentFacade.studentList.add(student8);
		student7.setSubjectList(subjectList7);
	}
}
