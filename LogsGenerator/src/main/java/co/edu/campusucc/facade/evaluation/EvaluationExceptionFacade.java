package co.edu.campusucc.facade.evaluation;

import java.util.ArrayList;
import java.util.List;

import co.edu.campusucc.response.ErrorUCC;
import co.edu.campusucc.response.ResponseData;
import co.edu.campusucc.response.ResponseHeader;
import co.edu.campusucc.response.ResponseUCC;

public class EvaluationExceptionFacade {
	
	private static List<ResponseUCC> responseList = new ArrayList<ResponseUCC>();
	
	public static ResponseUCC createResponse() {
		loadDataResponse();
		int indexRandom = (int) (Math.random() * EvaluationExceptionFacade.responseList.size());
		int finalIndex = (indexRandom % 9 == 0 ? indexRandom : 0);
		return EvaluationExceptionFacade.responseList.get(finalIndex);
	}
	
	private static void loadDataResponse() {
		
		ResponseUCC response = new ResponseUCC();
		ResponseHeader responseHeader = new ResponseHeader();
		responseHeader.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader.setStatus("200");
		response.setResponseHeaderObject(responseHeader);
		response.setResponseStatus("ok");
		ResponseData responseData = new ResponseData();
		responseData.setErrors(null);
		response.setResponseDataObject(responseData);
		responseList.add(response);
		
		ResponseUCC response2 = new ResponseUCC();
		ResponseHeader responseHeader2 = new ResponseHeader();
		responseHeader2.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader2.setStatus("400");
		response2.setResponseHeaderObject(responseHeader);
		response2.setResponseStatus("fail");
		ResponseData responseData2 = new ResponseData();
		ErrorUCC error = new ErrorUCC();
		error.setCode("400");
		error.setMessage("Bad Request");
		ArrayList<ErrorUCC> errorList = new ArrayList<ErrorUCC>();
		errorList.add(error);
		responseData2.setErrors(errorList);
		response2.setResponseDataObject(responseData);
		responseList.add(response2);
		
		ResponseUCC response3 = new ResponseUCC();
		ResponseHeader responseHeader3 = new ResponseHeader();
		responseHeader3.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader3.setStatus("400");
		response3.setResponseHeaderObject(responseHeader);
		response3.setResponseStatus("fail");
		ResponseData responseData3 = new ResponseData();
		ErrorUCC error3 = new ErrorUCC();
		error3.setCode("400");
		error3.setMessage("Bad Request");
		ArrayList<ErrorUCC> errorList3 = new ArrayList<ErrorUCC>();
		errorList3.add(error3);
		responseData3.setErrors(errorList);
		response3.setResponseDataObject(responseData2);
		responseList.add(response3);
		
		ResponseUCC response4 = new ResponseUCC();
		ResponseHeader responseHeader4 = new ResponseHeader();
		responseHeader4.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader4.setStatus("400");
		response3.setResponseHeaderObject(responseHeader);
		response3.setResponseStatus("fail");
		ResponseData responseData4 = new ResponseData();
		ErrorUCC error4 = new ErrorUCC();
		error4.setCode("401");
		error4.setMessage("Unauthorized");
		ArrayList<ErrorUCC> errorList4 = new ArrayList<ErrorUCC>();
		errorList4.add(error4);
		responseData4.setErrors(errorList);
		response4.setResponseDataObject(responseData4);
		responseList.add(response4);
		
		ResponseUCC response5 = new ResponseUCC();
		ResponseHeader responseHeader5 = new ResponseHeader();
		responseHeader5.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader5.setStatus("400");
		response5.setResponseHeaderObject(responseHeader);
		response5.setResponseStatus("fail");
		ResponseData responseData5 = new ResponseData();
		ErrorUCC error5 = new ErrorUCC();
		error5.setCode("403");
		error5.setMessage("Forbidden");
		ArrayList<ErrorUCC> errorList5 = new ArrayList<ErrorUCC>();
		errorList5.add(error4);
		responseData5.setErrors(errorList5);
		response5.setResponseDataObject(responseData5);
		responseList.add(response5);
		
		ResponseUCC response6 = new ResponseUCC();
		ResponseHeader responseHeader6 = new ResponseHeader();
		responseHeader6.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader6.setStatus("400");
		response6.setResponseHeaderObject(responseHeader);
		response6.setResponseStatus("fail");
		ResponseData responseData6 = new ResponseData();
		ErrorUCC error6 = new ErrorUCC();
		error6.setCode("403");
		error6.setMessage("Forbidden");
		ArrayList<ErrorUCC> errorList6 = new ArrayList<ErrorUCC>();
		errorList6.add(error6);
		responseData6.setErrors(errorList6);
		response6.setResponseDataObject(responseData6);
		responseList.add(response6);
		
		ResponseUCC response7 = new ResponseUCC();
		ResponseHeader responseHeader7 = new ResponseHeader();
		responseHeader7.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader7.setStatus("404");
		response7.setResponseHeaderObject(responseHeader7);
		response7.setResponseStatus("fail");
		ResponseData responseData7 = new ResponseData();
		ErrorUCC error7 = new ErrorUCC();
		error7.setCode("404");
		error7.setMessage("Not Found");
		ArrayList<ErrorUCC> errorList7 = new ArrayList<ErrorUCC>();
		errorList7.add(error7);
		responseData7.setErrors(errorList7);
		response7.setResponseDataObject(responseData7);
		responseList.add(response7);
		
		ResponseUCC response88 = new ResponseUCC();
		ResponseHeader responseHeader88 = new ResponseHeader();
		responseHeader88.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader88.setStatus("405");
		response88.setResponseHeaderObject(responseHeader88);
		response88.setResponseStatus("fail");
		ResponseData responseData88 = new ResponseData();
		ErrorUCC error88 = new ErrorUCC();
		error88.setCode("405");
		error88.setMessage("Method Not Allowed");
		ArrayList<ErrorUCC> errorList88 = new ArrayList<ErrorUCC>();
		errorList88.add(error88);
		responseData88.setErrors(errorList88);
		response88.setResponseDataObject(responseData88);
		responseList.add(response88);
		
		ResponseUCC response8 = new ResponseUCC();
		ResponseHeader responseHeader8 = new ResponseHeader();
		responseHeader8.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader8.setStatus("415");
		response8.setResponseHeaderObject(responseHeader8);
		response8.setResponseStatus("fail");
		ResponseData responseData8 = new ResponseData();
		ErrorUCC error8 = new ErrorUCC();
		error8.setCode("415");
		error8.setMessage("Unsupported Media Type");
		ArrayList<ErrorUCC> errorList8 = new ArrayList<ErrorUCC>();
		errorList8.add(error8);
		responseData8.setErrors(errorList8);
		response8.setResponseDataObject(responseData8);
		responseList.add(response8);
		
		ResponseUCC response9 = new ResponseUCC();
		ResponseHeader responseHeader9 = new ResponseHeader();
		responseHeader9.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader9.setStatus("500");
		response9.setResponseHeaderObject(responseHeader8);
		response9.setResponseStatus("fail");
		ResponseData responseData9 = new ResponseData();
		ErrorUCC error9 = new ErrorUCC();
		error9.setCode("description");
		error9.setCode("500");
		error9.setMessage("This field is required");
		ArrayList<ErrorUCC> errorList9 = new ArrayList<ErrorUCC>();
		errorList9.add(error9);
		responseData9.setErrors(errorList9);
		response9.setResponseDataObject(responseData9);
		responseList.add(response9);
		
		ResponseUCC response91 = new ResponseUCC();
		ResponseHeader responseHeader91 = new ResponseHeader();
		responseHeader91.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader91.setStatus("500");
		response91.setResponseHeaderObject(responseHeader8);
		response91.setResponseStatus("fail");
		ResponseData responseData91 = new ResponseData();
		ErrorUCC error911 = new ErrorUCC();
		error911.setCode("evaluationDate");
		error911.setCode("100");
		error911.setMessage("This field is required");
		ErrorUCC error912 = new ErrorUCC();
		error912.setCode("name");
		error912.setCode("100");
		error912.setMessage("This field is required");
		ErrorUCC error913 = new ErrorUCC();
		error913.setCode("qualification");
		error912.setCode("100");
		error912.setMessage("This field is incorrect");
		ArrayList<ErrorUCC> errorList91 = new ArrayList<ErrorUCC>();
		errorList91.add(error911);
		errorList91.add(error912);
		errorList91.add(error913);
		responseData9.setErrors(errorList91);
		response9.setResponseDataObject(responseData91);
		responseList.add(response91);
		
		ResponseUCC response92 = new ResponseUCC();
		ResponseHeader responseHeader92 = new ResponseHeader();
		responseHeader92.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader92.setStatus("500");
		response92.setResponseHeaderObject(responseHeader8);
		response92.setResponseStatus("fail");
		ResponseData responseData92 = new ResponseData();
		ErrorUCC error922 = new ErrorUCC();
		error912.setCode("name");
		error912.setCode("100");
		error912.setMessage("This field is required");
		ErrorUCC error923 = new ErrorUCC();
		error913.setCode("qualification");
		error912.setCode("100");
		error912.setMessage("This field is incorrect");
		ArrayList<ErrorUCC> errorList92 = new ArrayList<ErrorUCC>();
		errorList92.add(error922);
		errorList92.add(error923);
		responseData9.setErrors(errorList92);
		response92.setResponseDataObject(responseData92);
		responseList.add(response92);
		

		ResponseUCC response10 = new ResponseUCC();
		ResponseHeader responseHeader10 = new ResponseHeader();
		responseHeader10.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader10.setStatus("502");
		response10.setResponseHeaderObject(responseHeader10);
		response10.setResponseStatus("fail");
		ResponseData responseData10 = new ResponseData();
		ErrorUCC error10 = new ErrorUCC();
		error10.setCode("502");
		error10.setMessage("Bad Gateway");
		ArrayList<ErrorUCC> errorList10 = new ArrayList<ErrorUCC>();
		errorList10.add(error10);
		responseData10.setErrors(errorList10);
		response10.setResponseDataObject(responseData10);
		responseList.add(response10);
		
		ResponseUCC response11 = new ResponseUCC();
		ResponseHeader responseHeader11 = new ResponseHeader();
		responseHeader11.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader11.setStatus("503");
		response11.setResponseHeaderObject(responseHeader11);
		response11.setResponseStatus("fail");
		ResponseData responseData11 = new ResponseData();
		ErrorUCC error11 = new ErrorUCC();
		error11.setCode("503");
		error11.setMessage("Service Unavailable.");
		ArrayList<ErrorUCC> errorList11 = new ArrayList<ErrorUCC>();
		errorList11.add(error11);
		responseData11.setErrors(errorList11);
		response11.setResponseDataObject(responseData11);
		responseList.add(response11);
	}
}
