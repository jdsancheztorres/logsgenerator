package co.edu.campusucc.facade;
/**
 * Teacher CourseException associated to the simulation of logs.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class CourseException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CourseException(String msg) {
		super(msg);
	}

}
