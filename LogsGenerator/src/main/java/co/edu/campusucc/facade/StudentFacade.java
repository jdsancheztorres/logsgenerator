package co.edu.campusucc.facade;

import java.util.ArrayList;
import java.util.List;

import co.edu.campusucc.request.Student;
import co.edu.campusucc.request.Subject;
import co.edu.campusucc.response.ErrorUCC;
import co.edu.campusucc.response.ResponseData;
import co.edu.campusucc.response.ResponseHeader;
import co.edu.campusucc.response.ResponseUCC;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
/**
 * Student Facade associated to the simulation of logs.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class StudentFacade {
	
	private static List<Student> studentList = new ArrayList<Student>();
	private static List<ResponseUCC> responseList = new ArrayList<ResponseUCC>();	
	
	public static Student createRequest() {
		loadData();
		int indexRandom = (int) (Math.random() * StudentFacade.studentList.size());
		
		return StudentFacade.studentList.get(indexRandom);
	}
	
	public static ResponseUCC createResponse() {
		loadDataResponse();
		int indexRandom = (int) (Math.random() * StudentFacade.responseList.size());
		return StudentFacade.responseList.get(indexRandom);
	}
	
	private static void loadData() {
		Student student1 = new Student();
		student1.setAddress("cra 13 32 12");
		student1.setDocumentType("CC");
		student1.setIdStudent(1);
		student1.setLastName("Vargas");
		student1.setName("Martina");
		student1.setTelephone("3132342333");
		List<Subject> subjectList = new ArrayList<Subject>();
		Subject subject = new Subject();
		subject.setId(1);
		subject.setDescription("Gerencia de Proyectos de Tecnologia");
		subjectList.add(subject);
		student1.setSubjectList(subjectList);
		StudentFacade.studentList.add(student1);
		
		Student student2 = new Student();
		student2.setAddress("cra 13 32 13");
		student2.setDocumentType("CC");
		student2.setIdStudent(1);
		student2.setLastName("Giraldo");
		student2.setName("Rodrigo");
		student2.setTelephone("3173214123");
		List<Subject> subjectList2 = new ArrayList<Subject>();
		Subject subject2 = new Subject();
		subject2.setId(2);
		subject2.setDescription("Gobernabilidad de TI");
		subjectList2.add(subject2);
		Subject subject22 = new Subject();
		subject22.setId(2);
		subject22.setDescription("Tendencias Organizacionales");
		subjectList2.add(subject22);
		student2.setSubjectList(subjectList2);
		Subject subject23 = new Subject();
		subject23.setId(1);
		subject23.setDescription("Gerencia de Proyectos de Tecnologia");
		subjectList2.add(subject23);
		StudentFacade.studentList.add(student2);
		
		Student student3 = new Student();
		student3.setAddress("cra 1 0 13");
		student3.setDocumentType("CC");
		student3.setIdStudent(3);
		student3.setLastName("Coulet");
		student3.setName("Daniela");
		student3.setTelephone("313232453");
		List<Subject> subjectList3 = new ArrayList<Subject>();
		Subject subject3 = new Subject();
		subject3.setId(2);
		subject3.setDescription("Gobernabilidad de TI");
		subjectList3.add(subject3);
		Subject subject33 = new Subject();
		subject33.setId(4);
		subject33.setDescription("Paradigmas de investigación");
		subjectList3.add(subject33);
		student3.setSubjectList(subjectList3);
		StudentFacade.studentList.add(student3);
		
		Student student4 = new Student();
		student4.setAddress("cra 1 0 13");
		student4.setDocumentType("CC");
		student4.setIdStudent(4);
		student4.setLastName("Coulet");
		student4.setName("Daniela");
		student4.setTelephone("313232453");
		List<Subject> subjectList4 = new ArrayList<Subject>();
		Subject subject4 = new Subject();
		subject4.setId(2);
		subject4.setDescription("Gobernabilidad de TI");
		subjectList4.add(subject4);
		Subject subject41 = new Subject();
		subject41.setId(4);
		subject41.setDescription("Paradigmas de investigación");
		subjectList4.add(subject41);
		student3.setSubjectList(subjectList4);
		Subject subject42 = new Subject();
		subject42.setId(5);
		subject42.setDescription("Gestión de Riesgo de TI");
		subjectList4.add(subject42);
		student4.setSubjectList(subjectList4);
		StudentFacade.studentList.add(student4);
		
		Student student5 = new Student();
		student5.setAddress("cll 5 7 13");
		student5.setDocumentType("CC");
		student5.setIdStudent(5);
		student5.setLastName("Mesa");
		student5.setName("Carlos");
		student5.setTelephone("3172464354");
		List<Subject> subjectList5 = new ArrayList<Subject>();
		Subject subject5 = new Subject();
		subject5.setId(5);
		subject5.setDescription("Diseño de proyectos de interventoria");
		subjectList5.add(subject5);
		student5.setSubjectList(subjectList5);
		StudentFacade.studentList.add(student5);
		
		Student student6 = new Student();
		student6.setAddress("cll 5 7 13");
		student6.setDocumentType("CC");
		student6.setIdStudent(4);
		student6.setLastName("Coulet");
		student6.setName("Daniela");
		student6.setTelephone("313232453");
		List<Subject> subjectList6 = new ArrayList<Subject>();
		Subject subject6 = new Subject();
		subject6.setId(5);
		subject6.setDescription("Diseño de proyectos de interventoria");
		subjectList6.add(subject6);
		student6.setSubjectList(subjectList6);
		StudentFacade.studentList.add(student6);
		
		Student student7 = new Student();
		student7.setAddress("cra 56 42 24");
		student7.setDocumentType("CC");
		student7.setIdStudent(4);
		student7.setLastName("Romero");
		student7.setName("Brenda");
		student7.setTelephone("3182174238");
		List<Subject> subjectList7 = new ArrayList<Subject>();
		Subject subject7 = new Subject();
		subject7.setId(5);
		subject7.setDescription("Diseño de proyectos de interventoria");
		subjectList7.add(subject7);
		student7.setSubjectList(subjectList7);
		Subject subject71 = new Subject();
		subject71.setId(4);
		subject71.setDescription("Paradigmas de investigación");
		subjectList7.add(subject71);
		Subject subject72 = new Subject();
		subject72.setId(1);
		subject72.setDescription("Gerencia de Proyectos de Tecnologia");
		subjectList.add(subject72);
		StudentFacade.studentList.add(student7);
		student7.setSubjectList(subjectList7);
		
		Student student8 = new Student();
		student8.setAddress("cra 7 234 24");
		student8.setDocumentType("CC");
		student8.setIdStudent(4);
		student8.setLastName("Velazquez");
		student8.setName("Cain");
		student8.setTelephone("31723456432");
		List<Subject> subjectList8 = new ArrayList<Subject>();
		Subject subject8 = new Subject();
		subject8.setId(5);
		subject8.setDescription("Diseño de proyectos de interventoria");
		subjectList8.add(subject8);
		student8.setSubjectList(subjectList8);
		Subject subject81 = new Subject();
		subject81.setId(4);
		subject81.setDescription("Paradigmas de investigación");
		subjectList8.add(subject71);
		Subject subject82 = new Subject();
		subject82.setId(1);
		subject82.setDescription("Gerencia de Proyectos de Tecnologia");
		subjectList.add(subject82);
		StudentFacade.studentList.add(student8);
		student7.setSubjectList(subjectList7);
	}
	
	private static void loadDataResponse() {
		
		ResponseUCC response = new ResponseUCC();
		ResponseHeader responseHeader = new ResponseHeader();
		responseHeader.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader.setStatus("200");
		response.setResponseHeaderObject(responseHeader);
		response.setResponseStatus("ok");
		ResponseData responseData = new ResponseData();
		responseData.setErrors(null);
		response.setResponseDataObject(responseData);
		responseList.add(response);
		
		ResponseUCC response2 = new ResponseUCC();
		ResponseHeader responseHeader2 = new ResponseHeader();
		responseHeader2.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader2.setStatus("400");
		response2.setResponseHeaderObject(responseHeader);
		response2.setResponseStatus("fail");
		ResponseData responseData2 = new ResponseData();
		ErrorUCC error = new ErrorUCC();
		error.setCode("400");
		error.setMessage("Bad Request");
		ArrayList<ErrorUCC> errorList = new ArrayList<ErrorUCC>();
		errorList.add(error);
		responseData2.setErrors(errorList);
		response2.setResponseDataObject(responseData);
		responseList.add(response2);
		
		ResponseUCC response3 = new ResponseUCC();
		ResponseHeader responseHeader3 = new ResponseHeader();
		responseHeader3.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader3.setStatus("400");
		response3.setResponseHeaderObject(responseHeader);
		response3.setResponseStatus("fail");
		ResponseData responseData3 = new ResponseData();
		ErrorUCC error3 = new ErrorUCC();
		error3.setCode("400");
		error3.setMessage("Bad Request");
		ArrayList<ErrorUCC> errorList3 = new ArrayList<ErrorUCC>();
		errorList3.add(error3);
		responseData3.setErrors(errorList);
		response3.setResponseDataObject(responseData2);
		responseList.add(response3);
		
		ResponseUCC response4 = new ResponseUCC();
		ResponseHeader responseHeader4 = new ResponseHeader();
		responseHeader4.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader4.setStatus("400");
		response3.setResponseHeaderObject(responseHeader);
		response3.setResponseStatus("fail");
		ResponseData responseData4 = new ResponseData();
		ErrorUCC error4 = new ErrorUCC();
		error4.setCode("401");
		error4.setMessage("Unauthorized");
		ArrayList<ErrorUCC> errorList4 = new ArrayList<ErrorUCC>();
		errorList4.add(error4);
		responseData4.setErrors(errorList);
		response4.setResponseDataObject(responseData4);
		responseList.add(response4);
		
		ResponseUCC response5 = new ResponseUCC();
		ResponseHeader responseHeader5 = new ResponseHeader();
		responseHeader5.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader5.setStatus("400");
		response5.setResponseHeaderObject(responseHeader);
		response5.setResponseStatus("fail");
		ResponseData responseData5 = new ResponseData();
		ErrorUCC error5 = new ErrorUCC();
		error5.setCode("403");
		error5.setMessage("Forbidden");
		ArrayList<ErrorUCC> errorList5 = new ArrayList<ErrorUCC>();
		errorList5.add(error4);
		responseData5.setErrors(errorList5);
		response5.setResponseDataObject(responseData5);
		responseList.add(response5);
		
		ResponseUCC response6 = new ResponseUCC();
		ResponseHeader responseHeader6 = new ResponseHeader();
		responseHeader6.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader6.setStatus("400");
		response6.setResponseHeaderObject(responseHeader);
		response6.setResponseStatus("fail");
		ResponseData responseData6 = new ResponseData();
		ErrorUCC error6 = new ErrorUCC();
		error6.setCode("403");
		error6.setMessage("Forbidden");
		ArrayList<ErrorUCC> errorList6 = new ArrayList<ErrorUCC>();
		errorList6.add(error6);
		responseData6.setErrors(errorList6);
		response6.setResponseDataObject(responseData6);
		responseList.add(response6);
		
		ResponseUCC response7 = new ResponseUCC();
		ResponseHeader responseHeader7 = new ResponseHeader();
		responseHeader7.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader7.setStatus("404");
		response7.setResponseHeaderObject(responseHeader7);
		response7.setResponseStatus("fail");
		ResponseData responseData7 = new ResponseData();
		ErrorUCC error7 = new ErrorUCC();
		error7.setCode("404");
		error7.setMessage("Not Found");
		ArrayList<ErrorUCC> errorList7 = new ArrayList<ErrorUCC>();
		errorList7.add(error7);
		responseData7.setErrors(errorList7);
		response7.setResponseDataObject(responseData7);
		responseList.add(response7);
		
		ResponseUCC response88 = new ResponseUCC();
		ResponseHeader responseHeader88 = new ResponseHeader();
		responseHeader88.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader88.setStatus("405");
		response88.setResponseHeaderObject(responseHeader88);
		response88.setResponseStatus("fail");
		ResponseData responseData88 = new ResponseData();
		ErrorUCC error88 = new ErrorUCC();
		error88.setCode("405");
		error88.setMessage("Method Not Allowed");
		ArrayList<ErrorUCC> errorList88 = new ArrayList<ErrorUCC>();
		errorList88.add(error88);
		responseData88.setErrors(errorList88);
		response88.setResponseDataObject(responseData88);
		responseList.add(response88);
		
		ResponseUCC response8 = new ResponseUCC();
		ResponseHeader responseHeader8 = new ResponseHeader();
		responseHeader8.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader8.setStatus("415");
		response8.setResponseHeaderObject(responseHeader8);
		response8.setResponseStatus("fail");
		ResponseData responseData8 = new ResponseData();
		ErrorUCC error8 = new ErrorUCC();
		error8.setCode("415");
		error8.setMessage("Unsupported Media Type");
		ArrayList<ErrorUCC> errorList8 = new ArrayList<ErrorUCC>();
		errorList8.add(error8);
		responseData8.setErrors(errorList8);
		response8.setResponseDataObject(responseData8);
		responseList.add(response8);
		
		ResponseUCC response9 = new ResponseUCC();
		ResponseHeader responseHeader9 = new ResponseHeader();
		responseHeader9.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader9.setStatus("500");
		response9.setResponseHeaderObject(responseHeader8);
		response9.setResponseStatus("fail");
		ResponseData responseData9 = new ResponseData();
		ErrorUCC error9 = new ErrorUCC();
		error9.setCode("description");
		error9.setCode("500");
		error9.setMessage("This field is required");
		ArrayList<ErrorUCC> errorList9 = new ArrayList<ErrorUCC>();
		errorList9.add(error9);
		responseData9.setErrors(errorList9);
		response9.setResponseDataObject(responseData9);
		responseList.add(response9);
		
		ResponseUCC response91 = new ResponseUCC();
		ResponseHeader responseHeader91 = new ResponseHeader();
		responseHeader91.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader91.setStatus("500");
		response91.setResponseHeaderObject(responseHeader8);
		response91.setResponseStatus("fail");
		ResponseData responseData91 = new ResponseData();
		ErrorUCC error911 = new ErrorUCC();
		error911.setCode("evaluationDate");
		error911.setCode("100");
		error911.setMessage("This field is required");
		ErrorUCC error912 = new ErrorUCC();
		error912.setCode("name");
		error912.setCode("100");
		error912.setMessage("This field is required");
		ErrorUCC error913 = new ErrorUCC();
		error913.setCode("qualification");
		error912.setCode("100");
		error912.setMessage("This field is incorrect");
		ArrayList<ErrorUCC> errorList91 = new ArrayList<ErrorUCC>();
		errorList91.add(error911);
		errorList91.add(error912);
		errorList91.add(error913);
		responseData9.setErrors(errorList91);
		response9.setResponseDataObject(responseData91);
		responseList.add(response91);
		
		ResponseUCC response94 = new ResponseUCC();
		ResponseHeader responseHeader94 = new ResponseHeader();
		responseHeader94.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader94.setStatus("500");
		response94.setResponseHeaderObject(responseHeader8);
		response94.setResponseStatus("fail");
		ResponseData responseData94 = new ResponseData();
		ErrorUCC error941 = new ErrorUCC();
		error941.setCode("DocumentType");
		error941.setCode("100");
		error941.setMessage("This field is required");
		ErrorUCC error942 = new ErrorUCC();
		error942.setCode("name");
		error942.setCode("100");
		error942.setMessage("This field is required");
		ErrorUCC error943 = new ErrorUCC();
		error943.setCode("lastName");
		error943.setCode("100");
		error943.setMessage("This field is incorrect");
		ArrayList<ErrorUCC> errorList94 = new ArrayList<ErrorUCC>();
		errorList94.add(error941);
		errorList94.add(error942);
		errorList94.add(error943);
		responseData9.setErrors(errorList94);
		response94.setResponseDataObject(responseData94);
		responseList.add(response94);
		
		ResponseUCC response92 = new ResponseUCC();
		ResponseHeader responseHeader92 = new ResponseHeader();
		responseHeader92.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader92.setStatus("500");
		response92.setResponseHeaderObject(responseHeader8);
		response92.setResponseStatus("fail");
		ResponseData responseData92 = new ResponseData();
		ErrorUCC error921 = new ErrorUCC();
		error911.setField("telephone");
		error911.setCode("100");
		error911.setMessage("This field is incorrect");
		ErrorUCC error922 = new ErrorUCC();
		error912.setField("name");
		error912.setCode("100");
		error912.setMessage("This field is required");
		ErrorUCC error923 = new ErrorUCC();
		error913.setField("lastName");
		error912.setCode("100");
		error912.setMessage("This field is incorrect");
		ArrayList<ErrorUCC> errorList921 = new ArrayList<ErrorUCC>();
		errorList91.add(error921);
		errorList91.add(error922);
		errorList91.add(error923);
		responseData9.setErrors(errorList91);
		response9.setResponseDataObject(responseData91);
		responseList.add(response91);
		
		ResponseUCC response93 = new ResponseUCC();
		ResponseHeader responseHeader93 = new ResponseHeader();
		responseHeader93.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader93.setStatus("500");
		response93.setResponseHeaderObject(responseHeader8);
		response93.setResponseStatus("fail");
		ResponseData responseData93 = new ResponseData();
		ErrorUCC error932 = new ErrorUCC();
		error932.setCode("name");
		error932.setCode("100");
		error932.setMessage("This field is required");
		ErrorUCC error933 = new ErrorUCC();
		error933.setCode("qualification");
		error933.setCode("100");
		error933.setMessage("This field is incorrect");
		ArrayList<ErrorUCC> errorList93 = new ArrayList<ErrorUCC>();
		errorList93.add(error932);
		errorList93.add(error933);
		responseData9.setErrors(errorList93);
		response92.setResponseDataObject(responseData92);
		responseList.add(response92);
		

		ResponseUCC response10 = new ResponseUCC();
		ResponseHeader responseHeader10 = new ResponseHeader();
		responseHeader10.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader10.setStatus("502");
		response10.setResponseHeaderObject(responseHeader10);
		response10.setResponseStatus("fail");
		ResponseData responseData10 = new ResponseData();
		ErrorUCC error10 = new ErrorUCC();
		error10.setCode("502");
		error10.setMessage("Bad Gateway");
		ArrayList<ErrorUCC> errorList10 = new ArrayList<ErrorUCC>();
		errorList10.add(error10);
		responseData10.setErrors(errorList10);
		response10.setResponseDataObject(responseData10);
		responseList.add(response10);
		
		ResponseUCC response11 = new ResponseUCC();
		ResponseHeader responseHeader11 = new ResponseHeader();
		responseHeader11.setNow(Float.parseFloat(String.valueOf(Math.random() * 9999999999999L)));
		responseHeader11.setStatus("503");
		response11.setResponseHeaderObject(responseHeader11);
		response11.setResponseStatus("fail");
		ResponseData responseData11 = new ResponseData();
		ErrorUCC error11 = new ErrorUCC();
		error11.setCode("503");
		error11.setMessage("Service Unavailable.");
		ArrayList<ErrorUCC> errorList11 = new ArrayList<ErrorUCC>();
		errorList11.add(error11);
		responseData11.setErrors(errorList11);
		response11.setResponseDataObject(responseData11);
		responseList.add(response11);
		
	}
}
