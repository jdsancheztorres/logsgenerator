package co.edu.campusucc.facade;

import java.util.ArrayList;
import java.util.List;

import co.edu.campusucc.facade.course.CourseException;

public class ExceptionFacade {
	
	private static List<Exception> exceptionList = new ArrayList<Exception>();
	
	
	public static Exception generateException() {
		loadData();
		int indexRandom = (int) (Math.random() * ExceptionFacade.exceptionList.size());
		
		return ExceptionFacade.exceptionList.get(indexRandom);
	}
	
	private static void loadData() {
		exceptionList.add(new CourseException("Problems to create the course of history"));
	}
	
	public static void main(String args[]) {
		System.out.println(ExceptionFacade.generateException().getCause());
	}

}
