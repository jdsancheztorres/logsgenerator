package co.edu.campusucc.facade.course;

import java.util.ArrayList;
import java.util.List;

import co.edu.campusucc.request.Subject;
/**
 * CourseFacade class associated to the simulation of logs relate to
 * the available courses in the UCC.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class CourseFacade {
	
	private static List<Subject> courseList = new ArrayList<Subject>();
	
	
	public static Subject createRequest() {
		loadData();
		int indexRandom = (int) (Math.random() * CourseFacade.courseList.size());
		return CourseFacade.courseList.get(indexRandom);
	}
	
	private static void loadData() {
		Subject subject = new Subject();
		subject.setDescription("Gobernabilidad de TI");
		subject.setId(1);
		courseList.add(subject);
		Subject subject2 = new Subject();
		subject2.setDescription("Profundización I");
		subject2.setId(2);
		courseList.add(subject2);
		Subject subject3 = new Subject();
		subject3.setDescription("Evaluacion y Divulgacion de Proyectos");
		subject3.setId(3);
		courseList.add(subject3);
		Subject subject4 = new Subject();
		subject4.setDescription("Gestión del Conocimiento e Innovación");
		subject4.setId(4);
		courseList.add(subject4);
		Subject subject5 = new Subject();
		subject5.setDescription("Paradigmas de la investigacion");
		subject5.setId(5);
		courseList.add(subject5);
		Subject subject6 = new Subject();
		subject6.setDescription("Gerencia de proyectos de Tecnologia");
		subject6.setId(6);
		courseList.add(subject6);
		Subject subject7 = new Subject();
		subject7.setDescription("Diseño de proyectos de tecnología");
		subject7.setId(7);
		courseList.add(subject7);
	}
}
