package co.edu.campusucc;

import java.text.SimpleDateFormat;
import java.util.Date;

import co.edu.campusucc.request.UCCDateRequest;
/**
 * Utility class used in setting up a format type used in the creation of logs 
 * about Academic System Information.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class Util {
	
	public static String FORMAT_1 = "yyyy-MM-dd HH:mm:ss.SSS";
	public static String FORMAT_2 = "dd/MM/yyyy";
	
	public static String convertDateToString(UCCDateRequest ...request) {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_2);
        Date now = new Date();
        String strDate = sdf.format(now);
        return strDate;
	}
	
	public static void main(String args[]) {
		System.out.println(Util.convertDateToString());
	}
	
	private static UCCDateRequest createUCCDateRequest() {
		UCCDateRequest request = new UCCDateRequest();
		
		request.setDay(6);
		request.setMonth(4);
		request.setYear(2022);
		request.setHour(05);
		request.setMinute(22);
		request.setSecond(45);
		
		return request;
	}

}
