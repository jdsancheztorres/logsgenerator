package co.edu.campusucc;

import java.util.logging.Logger;

import org.springframework.scheduling.annotation.Scheduled;
/**
 * Teacher JDST_BAGF_UCC_Scheduler associated to the simulation of logs.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class JDST_BAGF_UCC_Scheduler {

    final Logger LOGGER = Logger.getLogger(JDST_BAGF_UCC_Scheduler.class.getName());

    @Scheduled(fixedRate = 1000)
    public void cronJob1() {
        LOGGER.info("Jose Danilo Sanchez Torres, Ingeniero de Sistemas, Magister en Educacion, estudiante de Maestria UCC:: ");
    }

    @Scheduled(fixedRate = 2000)
    public void cronJob2() {
        LOGGER.severe("Bertha Aidee Gonzalez Fabra, Ingeniera de Sistemas, estudiante de Maestria UCC:: ");
    }

    @Scheduled(fixedRate = 1500)
    public void cronJob3() {
        LOGGER.warning("Trabajo desarrollado ANALIZANDO BIG DATA – CASO DE ESTUDIO: “UCC LOGGING ARCHITECTURE, 2022");
    }
}
