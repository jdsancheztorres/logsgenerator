package co.edu.campusucc;

import java.util.logging.Logger;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import co.edu.campusucc.facade.course.CourseException;
import co.edu.campusucc.facade.course.CourseExceptionFacade;
import co.edu.campusucc.facade.course.CourseFacade;
import co.edu.campusucc.facade.evaluation.EvaluationExceptionFacade;
import co.edu.campusucc.facade.evaluation.EvaluationFacade;
import co.edu.campusucc.facade.student.StudentExceptionFacade;
import co.edu.campusucc.facade.student.StudentFacade;
import co.edu.campusucc.facade.teacher.TeacherExceptionFacade;
import co.edu.campusucc.facade.teacher.TeacherFacade;
import co.edu.campusucc.response.ResponseUCC;
/**
 * UCC_LOGS_GENERATOR component used in creating logs examples about Academic System Information.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
@Component	
@EnableScheduling
public class UCC_LOGS_GENERATOR {

	final Logger LOGGER = Logger.getLogger(UCC_LOGS_GENERATOR.class.getName());

	final String USER_INFO =  System.getProperty("user.name ") + " " 
			+ System.getProperty("os.name") + "-" + System.getProperty("os.version") + " ";
	
    @Scheduled(fixedRate = 1000)
    public void cronJob1() {
        LOGGER.info(USER_INFO + "co.com.josedanilosanz.logs.architecture.ucc.StudentController::findAll -- Start");
        String request = new Gson().toJson(StudentFacade.createRequest());
        LOGGER.info(USER_INFO + "co.com.josedanilosanz.logs.architecture.ucc.StudentController::findAll -- request:" + request);
        LOGGER.info(USER_INFO + "co.com.josedanilosanz.logs.architecture.ucc.StudentService::findAll -- Start");
        ResponseUCC response =  StudentExceptionFacade.createResponse();
        String responseFinal = new Gson().toJson(response);
        if(response.getResponseStatus().equalsIgnoreCase("fail")) {
        	LOGGER.severe(USER_INFO + "co.com.josedanilosanz.logs.architecture.ucc.StudentService::findAll -- response KO:" + responseFinal);
        } else {
        	LOGGER.info(USER_INFO + "co.com.josedanilosanz.logs.architecture.ucc.StudentService::findAll -- response OK:" + responseFinal);
        	LOGGER.info(USER_INFO + "co.com.josedanilosanz.logs.architecture.ucc.StudentService::findAll -- End");
            LOGGER.info(USER_INFO + "co.com.josedanilosanz.logs.architecture.ucc.StudentController::findAll -- End");
        } 
    }

    @Scheduled(fixedRate = 2500)
    public void cronJob2() {
        LOGGER.info(USER_INFO +"co.com.berthaaideegonzalez.logs.architecture.ucc.CourseController::create -- Start");
        String request = new Gson().toJson(CourseFacade.createRequest());
        LOGGER.info(USER_INFO +"co.com.berthaaideegonzalez.logs.architecture.ucc.CourseController::create -- request:" + request);
        LOGGER.info(USER_INFO +"co.com.berthaaideegonzalez.logs.architecture.ucc.CourseService::create -- Start");
        ResponseUCC response =  CourseExceptionFacade.createResponse();
        String responseFinal = new Gson().toJson(response);
        if(response.getResponseStatus().equalsIgnoreCase("fail")) {
        	LOGGER.severe(USER_INFO + "co.com.berthaaideegonzalez.logs.architecture.ucc.CourseService::create -- response KO:" + responseFinal);
        } else {
        	LOGGER.info(USER_INFO + "co.com.berthaaideegonzalez.logs.architecture.ucc.CourseService::create -- response OK:" + responseFinal);
        	LOGGER.info(USER_INFO +"co.com.berthaaideegonzalez.logs.architecture.ucc.CourseService::create -- End");
            LOGGER.info(USER_INFO +"co.com.berthaaideegonzalez.logs.architecture.ucc.CourseController::create -- End");
        }
        
    }
    
    @Scheduled(fixedRate = 5000)
    public void cronJob3() {
        LOGGER.info(USER_INFO +"co.com.lucianasanz.logs.architecture.ucc.TeacherController::create -- Start");
        String request = new Gson().toJson(TeacherFacade.createRequest());
        LOGGER.info(USER_INFO +"co.com.lucianasanz.logs.architecture.ucc.TeacherController::create -- request:" + request);
        LOGGER.info(USER_INFO +"co.com.lucianasanz.logs.architecture.ucc.TeacherService::create -- Start");
        LOGGER.severe(USER_INFO + new CourseException("Error in creating the teacher: " + request));
        ResponseUCC response =  TeacherExceptionFacade.createResponse();
        String responseFinal = new Gson().toJson(response);
        if(response.getResponseStatus().equalsIgnoreCase("fail")) {
        	LOGGER.severe(USER_INFO + "co.com.lucianasanz.logs.architecture.ucc.TeacherService::create -- response KO:" + responseFinal);
        } else {
        	LOGGER.info(USER_INFO + "co.com.lucianasanz.logs.architecture.ucc.TeacherService::create -- response OK:" + responseFinal);
        	LOGGER.info(USER_INFO +"co.com.lucianasanz.logs.architecture.ucc.TeacherService::create -- End");
            LOGGER.info(USER_INFO +"co.com.lucianasanz.logs.architecture.ucc.TeacherController::create -- End");
        }
    }
    
    @Scheduled(fixedRate = 1700)
    public void cronJob4() {
        LOGGER.info(USER_INFO +"co.com.lucianamedina.logs.architecture.ucc.EvaluationController::create -- Start");
        String request = new Gson().toJson(EvaluationFacade.createRequest());
        LOGGER.info(USER_INFO +"co.com.lucianamedina.logs.architecture.ucc.EvaluationController::create -- request:" + request);
        ResponseUCC response =  EvaluationExceptionFacade.createResponse();
        String responseFinal = new Gson().toJson(response);
        if(response.getResponseStatus().equalsIgnoreCase("fail")) {
        	LOGGER.severe(USER_INFO + "co.com.lucianamedina.logs.architecture.ucc.EvaluationController::create -- response KO:" + responseFinal);
        } else {
        	LOGGER.info(USER_INFO + "co.com.lucianamedina.logs.architecture.ucc.EvaluationService::create -- response OK:" + responseFinal);
        	LOGGER.info(USER_INFO +"co.com.lucianamedina.logs.architecture.ucc.EvaluationService::create -- End");
            LOGGER.info(USER_INFO +"co.com.lucianamedina.logs.architecture.ucc.EvaluationController::create -- End");
        }
    }
}
