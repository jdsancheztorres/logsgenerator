package co.edu.campusucc.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * ResponseBase class used for showing errors in the generated logs files.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class ResponseHeader {
	private float now;
	private String status;
	private String requestId;
}
