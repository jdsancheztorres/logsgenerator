package co.edu.campusucc.response;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
/**
 * ResponseBase class used for showing errors in the generated logs files.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class ResponseData {
	private ArrayList<ErrorUCC> errors = new ArrayList<ErrorUCC>();
}
