package co.edu.campusucc.response;

import lombok.Getter;
import lombok.Setter;

//https://www.ibm.com/docs/en/coss/3.10.2?topic=r-json-response-example-127
@Getter
@Setter
/**
 * ResponseBase class used for showing errors in the generated logs files.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class ResponseBase {
	private String responseStatus;
	private ResponseHeader ResponseHeaderObject;
	private ResponseData ResponseDataObject;
}
