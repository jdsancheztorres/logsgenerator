package co.edu.campusucc.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * ErrorUCC class used for showing errors in the generated logs files.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class ErrorUCC {
	private String field;
	private String code;
	private String message;
}
